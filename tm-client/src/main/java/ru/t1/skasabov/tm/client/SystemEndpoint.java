package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return systemEndpoint.getAbout(request);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return systemEndpoint.getVersion(request);
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(@NotNull final ApplicationSystemInfoRequest request) {
        return systemEndpoint.getSystemInfo(request);
    }

    @NotNull
    @Override
    public ApplicationHostNameResponse getHostName(@NotNull final ApplicationHostNameRequest request) {
        return systemEndpoint.getHostName(request);
    }

    @NotNull
    @Override
    public ApplicationUpdateSchemaResponse updateSchema(@NotNull final ApplicationUpdateSchemaRequest request) {
        return systemEndpoint.updateSchema(request);
    }

    @NotNull
    @Override
    public ApplicationDropSchemaResponse dropSchema(@NotNull final ApplicationDropSchemaRequest request) {
        return systemEndpoint.dropSchema(request);
    }

}
