package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        return projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        return projectEndpoint.changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProjects(@NotNull final ProjectClearRequest request) {
        return projectEndpoint.clearProjects(request);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return projectEndpoint.createProject(request);
    }

    @NotNull
    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull final ProjectGetByIdRequest request) {
        return projectEndpoint.getProjectById(request);
    }

    @NotNull
    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull final ProjectGetByIndexRequest request) {
        return projectEndpoint.getProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectListResponse listProjects(@NotNull final ProjectListRequest request) {
        return projectEndpoint.listProjects(request);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return projectEndpoint.removeProjectById(request);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return projectEndpoint.removeProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return projectEndpoint.updateProjectById(request);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return projectEndpoint.updateProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startProjectById(@NotNull final ProjectStartByIdRequest request) {
        return projectEndpoint.startProjectById(request);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return projectEndpoint.startProjectByIndex(request);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeProjectById(@NotNull final ProjectCompleteByIdRequest request) {
        return projectEndpoint.completeProjectById(request);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeProjectByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return projectEndpoint.completeProjectByIndex(request);
    }

}
