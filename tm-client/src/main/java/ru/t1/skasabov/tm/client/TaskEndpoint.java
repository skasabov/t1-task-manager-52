package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    @Override
    public TaskGetByProjectIdResponse findAllByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        return taskEndpoint.findAllByProjectId(request);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return taskEndpoint.bindTaskToProject(request);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        return taskEndpoint.unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        return taskEndpoint.changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        return taskEndpoint.changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        return taskEndpoint.clearTasks(request);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return taskEndpoint.createTask(request);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        return taskEndpoint.getTaskById(request);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        return taskEndpoint.getTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        return taskEndpoint.listTasks(request);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return taskEndpoint.removeTaskById(request);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return taskEndpoint.removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return taskEndpoint.updateTaskById(request);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return taskEndpoint.updateTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        return taskEndpoint.startTaskById(request);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        return taskEndpoint.startTaskByIndex(request);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        return taskEndpoint.completeTaskById(request);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return taskEndpoint.completeTaskByIndex(request);
    }

}
