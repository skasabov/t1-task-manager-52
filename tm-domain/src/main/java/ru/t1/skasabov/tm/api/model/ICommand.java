package ru.t1.skasabov.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void execute();

}
