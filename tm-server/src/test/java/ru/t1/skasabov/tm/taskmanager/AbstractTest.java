package ru.t1.skasabov.tm.taskmanager;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.BeforeClass;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.service.ConnectionService;
import ru.t1.skasabov.tm.service.PropertyService;

public abstract class AbstractTest {

    @NotNull
    protected static final IPropertyService propertyService = new PropertyService();

    @NotNull
    protected static final IConnectionService connectionService = new ConnectionService(propertyService);

    @BeforeClass
    @SneakyThrows
    public static void init() {
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
        liquibase.update("schema");
    }

}
