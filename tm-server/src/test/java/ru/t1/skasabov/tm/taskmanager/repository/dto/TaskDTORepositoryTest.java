package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.dto.ProjectDTORepository;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

public class TaskDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private static UserDTO userOne;

    @NotNull
    private static UserDTO userTwo;

    @NotNull
    private ProjectDTO projectOne;

    @NotNull
    private ProjectDTO projectTwo;

    @NotNull
    private ITaskDTORepository taskRepository;

    @NotNull
    private EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        final long currentTime = System.currentTimeMillis();
        entityManager = connectionService.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        taskRepository = new TaskDTORepository(entityManager);
        entityManager.getTransaction().begin();
        userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        projectOne = new ProjectDTO();
        projectOne.setUserId(USER_ID_ONE);
        projectOne.setName("project_one");
        projectTwo = new ProjectDTO();
        projectTwo.setUserId(USER_ID_TWO);
        projectTwo.setName("project_two");
        projectRepository.add(projectOne);
        projectRepository.add(projectTwo);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            task.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) task.setStatus(Status.COMPLETED);
            else if (i < 7) task.setStatus(Status.IN_PROGRESS);
            else task.setStatus(Status.NOT_STARTED);
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            } else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskRepository.add(task);
        }
    }

    @Test
    public void testUpdate() {
        @NotNull final TaskDTO task = taskRepository.findAll(USER_ID_TWO).get(0);
        task.setName("Test Task One");
        task.setDescription("Test Description One");
        taskRepository.update(task);
        @Nullable final TaskDTO actualTask = taskRepository.findOneById(USER_ID_TWO, task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(USER_ID_TWO, actualTask.getUserId());
        Assert.assertEquals("Test Task One", actualTask.getName());
        Assert.assertEquals("Test Description One", actualTask.getDescription());
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 4;
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_ONE);
            actualTasks.add(task);
        }
        taskRepository.addAll(actualTasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test Task " + i);
            task.setUserId(USER_ID_TWO);
            actualTasks.add(task);
        }
        taskRepository.set(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testClearAll() {
        taskRepository.removeAll();
        Assert.assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        taskRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<TaskDTO> taskList = taskRepository.findAll(USER_ID_TWO);
        taskRepository.removeAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<TaskDTO> taskList = taskRepository.findAll();
        Assert.assertEquals(taskList.size(), taskRepository.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final List<TaskDTO> taskSortList = taskRepository.findAllSortByName();
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        actualTasks.add(1, actualTasks.get(NUMBER_OF_ENTRIES - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final List<TaskDTO> taskSortList = taskRepository.findAllSortByCreated();
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final List<Status> statusList = taskRepository.findAllSortByStatus()
                .stream().map(TaskDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            if (i < 4) actualStatuses.add(Status.COMPLETED);
            else if (i < 7) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<TaskDTO> taskList = taskRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final List<TaskDTO> taskSortList = taskRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final List<TaskDTO> taskSortList = taskRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<TaskDTO> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final List<Status> statusList = taskRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(TaskDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindById() {
        @NotNull final TaskDTO task = taskRepository.findAll().get(0);
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        @Nullable final TaskDTO actualTask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final TaskDTO task = taskRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final TaskDTO actualTask = taskRepository.findOneById(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final TaskDTO task = taskRepository.findAll().get(0);
        @Nullable final TaskDTO actualTask = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIndexTaskNotFound() {
        Assert.assertNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final TaskDTO task = taskRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final TaskDTO actualTask = taskRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUserId(), actualTask.getUserId());
    }

    @Test
    public void testFindByIndexForUserTaskNotFound() {
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_ONE, NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_ONE);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Test Task");
        task.setUserId(USER_ID_TWO);
        taskRepository.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(invalidId));
        Assert.assertTrue(taskRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final TaskDTO task = taskRepository.findAll().get(0);
        taskRepository.removeOne(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        taskRepository.removeOneById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        taskRepository.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        taskRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize(USER_ID_TWO) - 1;
        taskRepository.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testFindAllByProjectIdForUser() {
        @NotNull final List<TaskDTO> tasksOne = taskRepository.findAll(USER_ID_ONE);
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByEmptyProjectIdForUser() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, "");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectIdForUser() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<TaskDTO> tasksOne = taskRepository.findAll(USER_ID_ONE);
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByEmptyProjectId() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId("");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId("some_id");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByUsers() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByUsers();
        Assert.assertEquals(taskRepository.getSize(), tasks.size());
    }

    @Test
    public void testFindAllByListUsers() {
        @NotNull final List<UserDTO> users = Arrays.asList(userOne, userTwo);
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByUsers(users);
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());
    }

    @Test
    public void testFindAllByEmptyListUsers() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByUsers(Collections.emptyList());
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void testFindAllByProjects() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects();
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());
    }

    @Test
    public void testFindAllByUserProjects() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, tasks.size());
    }

    @Test
    public void testFindAllByListProjects() {
        @NotNull final List<ProjectDTO> projects = Arrays.asList(projectOne, projectTwo);
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects(projects);
        Assert.assertEquals(NUMBER_OF_ENTRIES, tasks.size());
    }

    @Test
    public void testFindAllByEmptyListProjects() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects(Collections.emptyList());
        Assert.assertEquals(0, tasks.size());
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
