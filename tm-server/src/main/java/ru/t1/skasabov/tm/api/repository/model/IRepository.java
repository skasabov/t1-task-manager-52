package ru.t1.skasabov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    List<M> findAll();

    @NotNull
    Collection<M> addAll(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    Boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    long getSize();

    void removeOne(@NotNull M model);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collection);

    void removeAll();

}
