package ru.t1.skasabov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_LIQUIBASE_DEFAULT = "1234567890";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_CACHE_USE_SECOND_LEVEL_CACHE_KEY = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_QUERY_CACHE_KEY = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_KEY = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_KEY = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_KEY = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_CACHE_REGION_FACTORY_CLASS_KEY = "database.cache.region.factory_class";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String SERVER_HOST_KEY_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SERVER_PORT_KEY_DEFAULT = "8080";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_LIQUIBASE_KEY = "password.liquibase";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "5363453453";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY_DEFAULT = "10800";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/taskmanager";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_CACHE_USE_SECOND_LEVEL_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_CACHE_USE_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_DEFAULT = "tm";

    @NotNull
    private static final String DATABASE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String DATABASE_CACHE_REGION_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getPasswordLiquibase() {
        return getStringValue(PASSWORD_LIQUIBASE_KEY, PASSWORD_LIQUIBASE_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUserName() {
        return getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseSecondLevelCache() {
        return getStringValue(
                DATABASE_CACHE_USE_SECOND_LEVEL_CACHE_KEY,
                DATABASE_CACHE_USE_SECOND_LEVEL_CACHE_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseQueryCache() {
        return getStringValue(DATABASE_CACHE_USE_QUERY_CACHE_KEY, DATABASE_CACHE_USE_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseMinimalPuts() {
        return getStringValue(DATABASE_CACHE_USE_MINIMAL_PUTS_KEY, DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionPrefix() {
        return getStringValue(DATABASE_CACHE_REGION_PREFIX_KEY, DATABASE_CACHE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseCacheProviderConfigurationFileResourcePath() {
        return getStringValue(
                DATABASE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_KEY,
                DATABASE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_DEFAULT
        );
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionFactoryClass() {
        return getStringValue(DATABASE_CACHE_REGION_FACTORY_CLASS_KEY, DATABASE_CACHE_REGION_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_KEY_DEFAULT);
    }

    @NotNull
    private String envKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = envKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}
