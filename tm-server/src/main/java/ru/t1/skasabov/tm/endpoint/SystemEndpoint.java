package ru.t1.skasabov.tm.endpoint;

import liquibase.Liquibase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.component.Backup;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.exception.field.PasswordEmptyException;
import ru.t1.skasabov.tm.exception.user.PasswordIncorrectException;
import ru.t1.skasabov.tm.util.FormatUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return getServiceLocator().getConnectionService();
    }

    @NotNull
    private Backup getBackup() {
        return getServiceLocator().getBackup();
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationSystemInfoResponse getSystemInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationSystemInfoRequest request
    ) {
        @NotNull final ApplicationSystemInfoResponse response = new ApplicationSystemInfoResponse();
        @NotNull final Runtime runtime = Runtime.getRuntime();
        response.setAvailableProcessors(runtime.availableProcessors());
        final long freeMemory = runtime.freeMemory();
        response.setFreeMemory(FormatUtil.format(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        response.setMaximumMemory(maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        response.setTotalMemory(FormatUtil.format(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        response.setUsageMemory(FormatUtil.format(usageMemory));
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationHostNameResponse getHostName(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationHostNameRequest request
    ) {
        @NotNull final ApplicationHostNameResponse response = new ApplicationHostNameResponse();
        response.setHostName(InetAddress.getLocalHost().getHostName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationDropSchemaResponse dropSchema(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationDropSchemaRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @Nullable final String password = request.getPassword();
        @Nullable final String rightPassword = propertyService.getPasswordLiquibase();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (!password.equals(rightPassword)) throw new PasswordIncorrectException();
        getBackup().stop();
        @NotNull final ApplicationDropSchemaResponse response = new ApplicationDropSchemaResponse();
        @NotNull final Liquibase liquibase = getConnectionService().getLiquibase();
        liquibase.dropAll();
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationUpdateSchemaResponse updateSchema(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationUpdateSchemaRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @Nullable final String password = request.getPassword();
        @Nullable final String rightPassword = propertyService.getPasswordLiquibase();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (!password.equals(rightPassword)) throw new PasswordIncorrectException();
        getBackup().stop();
        @NotNull final ApplicationUpdateSchemaResponse response = new ApplicationUpdateSchemaResponse();
        @NotNull final Liquibase liquibase = getConnectionService().getLiquibase();
        liquibase.update("schema");
        return response;
    }

}
