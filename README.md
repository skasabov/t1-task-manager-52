# TASK MANAGER

## DEVELOPER INFO

**NAME**: Stas Kasabov

**E-MAIL**: stas@kasabov.ru

**E-MAIL**: stkasabov@yandex.ru

## SOFTWARE

**OS**: Windows 10 Pro 21H2

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i5-9600K

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```shell
mvn clean install
```

## RUN CLIENT PROGRAM

```shell
java -jar tm-client.jar
```

## TEST CLIENT PROGRAM

```shell
java -jar tm-server.jar
mvn test -P INTEGRATION
```

## RUN SERVER PROGRAM

```shell
java -jar tm-server.jar
```

## TEST SERVER PROGRAM

```shell
mvn test
```

## RUN LOGGER PROGRAM

```shell
java -jar tm-logger.jar
```

## TEST LOGGER PROGRAM

```shell
mvn test
```

## BUILD DOCKER CLUSTER

```shell
docker build -t tm-server tm-server
docker build -t tm-logger tm-logger
docker build -t tm-broker tm-broker
docker build -t tm-balancer tm-balancer
docker build -t tm-client tm-client
```

## RUN DOCKER CLUSTER

```shell
docker-compose up -d
```
